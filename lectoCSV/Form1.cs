﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.IO;

namespace lectoCSV
{
    public partial class Form1 : Form
    {
        public Form1()
        {
            InitializeComponent();
        }

        private void Form1_Load(object sender, EventArgs e)
        {
            //Agrego un comentario

            //Quiero abrir un archivo de texto
            StreamReader f = new StreamReader("esculturasarteenlaciudad2.csv", System.Text.Encoding.Default);

            //Quiero leer linea a linea el archivo de texto
            //Cada vez que lea una linea, agregar dicha
            // linea a los items del lstResultado
            string linea; //Variable de tipo string
            string[] vlinea; //Variable vector de strings
            string dato; //Para jugar con los datos

            int contador = 0;

            while ((linea = f.ReadLine())!=null)
            {
                if(contador>0){
                    //Extraigo de la linea, un vector de partes
                    vlinea=linea.Split(',');
                    //Vuelco el nombre de la escultura en el listado
                    //dato = vlinea[0].ToLower(); //En minusculas
                    //dato = vlinea[0].ToUpper(); //En mayusculas
                    //dato = vlinea[0].Substring(1, vlinea[0].Length-2);
                    dato = vlinea[2].ToUpper().Trim().Substring(4,4);

                    dato=vlinea[0].Replace("\"", "");
                   

                    lstResultado.Items.Add(dato);
                }
                contador++;
            }



            //Cerraremos el fichero de texto
            f.Close();
        }
    }
}
